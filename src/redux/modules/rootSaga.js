import { all, takeLatest } from 'redux-saga/effects';
import { userSagas, userTypes } from './user';

export default function* root() {
  return yield all([
    takeLatest(userTypes.USER_REQUEST, userSagas.getUser),
    takeLatest(userTypes.USER_REPO_REQUEST, userSagas.getRepo),
    takeLatest(userTypes.USER_STARRED_REQUEST, userSagas.getStarred),
  ]);
}
