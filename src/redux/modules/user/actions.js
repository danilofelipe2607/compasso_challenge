import { action } from 'typesafe-actions';
import * as userTypes from './types';

export const userRequest = (data) => action(userTypes.USER_REQUEST, data);
export const userSucess = (data) => action(userTypes.USER_SUCESS, data);
export const userError = () => action(userTypes.USER_ERROR);
export const userInitial = () => action(userTypes.USER_INITIAL_STATE);

export const repoRequest = (data) => action(userTypes.USER_REPO_REQUEST, data);
export const repoSucess = (data) => action(userTypes.USER_REPO_SUCESS, data);
export const starredRequest = (data) => action(userTypes.USER_STARRED_REQUEST, data);
export const starredSucess = (data) => action(userTypes.USER_STARRED_SUCESS, data);
