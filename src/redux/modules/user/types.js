export const USER_REQUEST = '@user/USER_REQUEST';
export const USER_INITIAL_STATE = '@user/USER_INITIAL_STATE';
export const USER_SUCESS = '@user/USER_SUCESS';
export const USER_ERROR = '@user/USER_ERROR';

export const USER_REPO_REQUEST = '@user/REPO_REQUEST';
export const USER_REPO_SUCESS = '@user/REPO_SUCESS';
export const USER_STARRED_REQUEST = '@user/STARRED_REQUEST';
export const USER_STARRED_SUCESS = '@user/STARRED_SUCESS';
