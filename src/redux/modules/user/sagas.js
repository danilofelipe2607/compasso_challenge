import { call, put } from 'redux-saga/effects';
import { userError, userSucess, repoSucess, starredSucess } from './actions';
import gitHub from '../../../api/github';

export function* getUser(values) {
  try {
    const { data } = yield call(gitHub.get, `/users/${values.payload}`);

    yield put(userSucess(data));
  } catch (error) {
    yield put(userError());
  }
}

export function* getRepo(values) {
  try {
    const { data } = yield call(gitHub.get, `/users/${values.payload}/repos`);
    yield put(repoSucess(data));
  } catch (error) {
    yield put(userError());
  }
}

export function* getStarred(values) {
  try {
    const { data } = yield call(gitHub.get, `/users/${values.payload}/starred`);
    yield put(starredSucess(data));
  } catch (error) {
    yield put(userError());
  }
}
