import * as userTypes from './types';

const INITIAL_STATE = {
  data: {
    user: null,
    repo: null,
    starred: null,
  },
  loading: false,
  error: false,
};

export default (state = INITIAL_STATE, action) => {
  const { type, payload } = action;

  switch (type) {
    case userTypes.USER_REQUEST:
      return { ...state, loading: true };
    case userTypes.USER_SUCESS:
      return { ...state, data: { ...state.data, user: payload }, loading: false, error: false };
    case userTypes.USER_ERROR:
      return { ...state, error: true };
    case userTypes.USER_REPO_REQUEST:
      return { ...state, loading: true };
    case userTypes.USER_REPO_SUCESS:
      return { ...state, data: { ...state.data, repo: payload }, loading: false, error: false };
    case userTypes.USER_STARRED_REQUEST:
      return { ...state, loading: true };
    case userTypes.USER_STARRED_SUCESS:
      return { ...state, data: { ...state.data, starred: payload }, loading: false, error: false };
    case userTypes.USER_INITIAL_STATE:
      return { ...state, data: { ...state.data, user: null }, loading: false, error: false };
    default:
      return state;
  }
};
