import React from 'react';
import { AiFillLinkedin, AiFillGithub } from 'react-icons/ai';
import { FaReact } from 'react-icons/fa';
import CardStyles from './styles';

function Card({ user, edit, repo, starred, open }) {
  if (!user) {
    return (
      <div style={{ textAlign: 'center', marginTop: '50px' }}>
        {' '}
        <h1>Página não encontrada!</h1>
      </div>
    );
  }
  const { avatar_url, name, bio, location, html_url, login } = user;
  return (
    <CardStyles>
      <div className="d-flex p-2">
        <div className="card">
          <div className="row">
            <div className="col-md-3">
              <img src={avatar_url} alt="Avatar" />
            </div>
            <div className="col-md-6 mt-3">
              <h2 className="card-title">{name}</h2>
              <div className="card-text">
                <div className="text-sm">{bio}</div>
                <div className="text-sm">{location}</div>
                <div className="mt-3">
                  <FaReact /> <AiFillLinkedin /> <AiFillGithub />
                </div>
              </div>
              {edit && (
                <div className="d-flex flex-row justify-content-center">
                  <button
                    type="button"
                    onClick={() => window.open(html_url, '_blank')}
                    className="col-md-6 btn btn-primary mr-2"
                  >
                    Visitar GitHub
                  </button>
                  <button
                    type="button"
                    onClick={() => edit(login)}
                    className="col-md-6 btn btn-primary ml-2"
                  >
                    Visualizar Perfil
                  </button>
                </div>
              )}
              {!edit && repo && (
                <div className="row">
                  <div className="col-md-6">
                    <button type="button" className="btn btn-primary" onClick={() => open(repo)}>
                      Repos <span className="badge bg-outline-secondary">{repo?.length}</span>
                    </button>
                  </div>
                  <div className="col-md-6">
                    <button type="button" className="btn btn-primary" onClick={() => open(starred)}>
                      Starred
                      <span className="badge bg-outline-secondary">{starred?.length}</span>
                    </button>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </CardStyles>
  );
}

export default Card;
