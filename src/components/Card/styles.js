import styled from 'styled-components';

const CardStyles = styled.div`
  height: auto;
  width: auto;
  display: flex;
  align-items: center;
  flex-direction: column;
  padding: 2.5rem 0;
  text-align: center;

  img {
    width: 80%;
    max-width: 80%;
    height: auto;
    border-radius: 20rem;
    align-items: center;
    margin: 0.5rem;
  }

  p {
    font-size: 1rem;
    margin-top: 2rem;
    margin-right: 0.5rem;
  }

  button {
    margin-top: 2rem;
    margin-left: 1rem;
    margin-right: 1rem;
    margin-bottom: 2rem;
    align-items: center;

    text-align: center;
    padding: 0.2rem;
    border-radius: 0.5rem;
    font-size: 1.5rem;
  }
`;

export default CardStyles;
