import styled from 'styled-components';

const ModalStyles = styled.div`
  b {
    text-align: center;
    font-size: 18px;
    font-weight: bold;
  }
  a {
    text-align: center;
    font-weight: bold;
    font-size: 15px;
    white-space: nowrap;
    line-height: 1;
  }
`;

export default ModalStyles;
