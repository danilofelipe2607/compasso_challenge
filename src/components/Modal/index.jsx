import React from 'react';
import ModalStyles from './styles';

function Modal({ name, title, dataRepo }) {
  const lista = dataRepo.map((repo) => (
    <div className="card" key={repo.id}>
      <div className="card-title">
        <b> {repo.name}</b> <a href={repo.html_url}> {repo.html_url}</a>
      </div>
    </div>
  ));

  return (
    <ModalStyles>
      <div className="modal fade" id={name} aria-labelledby={name} aria-hidden="true">
        <div className="modal-dialog modal-dialog-scrollable">
          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title" id={name}>
                {title}
              </h4>
            </div>
            <div className="modal-body">{lista}</div>
          </div>
        </div>
      </div>
    </ModalStyles>
  );
}

export default Modal;
