import React from 'react';
import { HashRouter } from 'react-router-dom';
import AppStyles from './assets/styles/styles';

import Routes from './routes';

export default function App() {
  return (
    <HashRouter>
      <AppStyles />
      <Routes />
    </HashRouter>
  );
}
