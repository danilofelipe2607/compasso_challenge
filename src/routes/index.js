import React from 'react';
import { BrowserRouter, HashRouter, Route, Router, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { Home, User } from '../views';

const hist = createBrowserHistory();
export default function Routes() {
  return (
    <BrowserRouter>
      <Router history={hist}>
        <HashRouter>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/:id" component={User} />
          </Switch>
        </HashRouter>
      </Router>
    </BrowserRouter>
  );
}
