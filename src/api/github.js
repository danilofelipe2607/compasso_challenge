import axios from 'axios';

const gitHub = axios.create({
  headers: {
    'Content-Type': 'application/json',
  },
  baseURL: 'https://api.github.com',
});

export default gitHub;
