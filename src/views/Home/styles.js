import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  padding: 2.5rem 0;
  width: 100%;

  img {
    height: 180px;
  }

  input {
    height: 100%;
    width: 100%;
    padding: 0 2rem;
    border-radius: 0.8rem;
    border-width: 0.05rem;
    font-size: 1.8rem;
  }
  button {
    border-radius: 0.8rem;
    font-size: 2rem;
  }
`;

export default Container;
