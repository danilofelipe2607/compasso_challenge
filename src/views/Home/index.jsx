import React, { useCallback, useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { FcSearch } from 'react-icons/fc';
import { useDispatch, useSelector } from 'react-redux';

import Container from './styles';
import img from '../../assets/img/github-mark.png';
import { userRequest, userInitial } from '../../redux/modules/user/actions';
import CardUser from '../../components/Card';

function Home() {
  const dispatch = useDispatch();
  const [nomePesquisa, setNomePesquisa] = useState(null);
  const {
    data: { user },
    error,
  } = useSelector((state) => state.userReducer);
  const history = useHistory();

  // eslint-disable-next-line no-alert
  const showError = () => alert('Usuário não encontrado!');

  useEffect(() => {
    dispatch(userInitial());

    if (error) {
      showError();
    }
  }, [dispatch, error]);

  const handleSubmit = useCallback(
    (e) => {
      dispatch(userRequest(nomePesquisa));
      e.preventDefault();
    },
    [nomePesquisa, dispatch],
  );

  const handleEdit = (value) => {
    history.push(`/${value}`);
  };

  return (
    <Container>
      <img src={img} alt="GitHub" />
      <form className="d-flex p-2" onSubmit={handleSubmit}>
        <div className="col-10">
          <input
            className="btn btn-outline-primary"
            onChange={(e) => setNomePesquisa(e.target.value)}
            type="text"
            required
            placeholder="Digite um usuário do GitHub"
          />
        </div>
        <div className="col-2">
          <button type="submit" className="btn btn-outline-primary ">
            <FcSearch />
          </button>
        </div>
      </form>

      {user && <CardUser user={user} edit={(value) => handleEdit(value)} />}
    </Container>
  );
}

export default Home;
