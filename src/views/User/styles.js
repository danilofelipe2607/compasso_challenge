import styled from 'styled-components';

const UserStyles = styled.div`
  width: 100%;
  max-width: 1000px;
  min-height: 95vh;
  margin: 0 auto;
  padding: 2rem 0;
  display: flex;
  align-items: center;
  flex-direction: column;

  a {
    text-align: center;
    width: 20%;
  }

  main {
    width: 100%;
  }

  main img {
    width: 100%;
    height: auto;
    border-radius: 10rem;
    align-items: center;
    margin: 0.5rem;
  }
`;

export default UserStyles;
