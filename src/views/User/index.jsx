import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import $ from 'jquery';

import UserStyles from './styles';
import Card from '../../components/Card';
import Modal from '../../components/Modal';
import { repoRequest, starredRequest } from '../../redux/modules/user/actions';

function User(item) {
  const [dataRepo, setDataRepo] = useState([]);
  const dispath = useDispatch();

  const { user, repo, starred } = useSelector((state) => state.userReducer.data);

  useEffect(() => {
    dispath(repoRequest(item.match.params.id));
    dispath(starredRequest(item.match.params.id));
  }, [item.match.params.id, dispath]);

  function modalOpen(modal) {
    $(modal).modal('show');
  }

  function openModal(values) {
    setDataRepo(values);
    modalOpen('#modalRepo');
  }
  return (
    <>
      <Modal name="modalRepo" title="Repositórios" dataRepo={dataRepo} />

      <UserStyles>
        <>
          <a href="#/" className="btn btn-primary btn-lg">
            Voltar
          </a>
          <main>
            <Card user={user} repo={repo} starred={starred} open={(values) => openModal(values)} />
          </main>
        </>
      </UserStyles>
    </>
  );
}

export default User;
