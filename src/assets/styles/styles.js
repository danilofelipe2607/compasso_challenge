import { createGlobalStyle } from 'styled-components';

const AppStyles = createGlobalStyle`

* {
  margin: 0;
  padding: 0;
  outline: 0;
  box-sizing: border-box;


}
:root{
   font-size: 75.0%;
 }

body {
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  font-size: 20px;


}
body html #root {
  height: 100%;
}

`;

export default AppStyles;
